import os
import sys
import socket
from Config.Configuration import Configuration


""" FUNCTIONS """

def send_server_request(client_request):
    # Create a udp socket
    ip_address = Configuration.read_config_value("SETTINGS", "SERVER_IP", "ClientConfiguration.conf")
    port = Configuration.read_config_value("SETTINGS", "SERVER_PORT", "ClientConfiguration.conf")
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = (ip_address, port)

    # Send request // Todo: Removed exception... think about that:
    print('Sending request [ {!r} ]'.format(client_request))
    sent = sock.sendto(client_request.encode(), server_address)
    return sock


def wait_server_response(sock):
    print('Waiting for response...')
    try:
        data, server = sock.recvfrom(4096)
        data = data.decode('utf-8')
        print("Received data: {}".format(data))

    except ConnectionResetError:
        print("There were problems in the server side, check it and try again...")

    print('Closing socket')
    sock.close()


def check_calibration_command(command):
    # Get the main calibration commands
    start_auto_calibrate_command = Configuration.read_config_value("CLIENT_COMMANDS", "AUTOMATIC_CALIBRATE_COMMAND",
                                                                   "ClientConfiguration.conf")
    start_manual_calibrate_command = Configuration.read_config_value("CLIENT_COMMANDS", "MANUAL_CALIBRATE_COMMAND",
                                                                     "ClientConfiguration.conf")

    # Start special process. Calibration.
    if command == start_auto_calibrate_command or command == start_manual_calibrate_command:
        # Send command server
        send_server_request(command)

        # Define used keys for keypress event and boolean calibration state variables
        is_calibrating_step_1 = True
        is_calibrating_step_2 = True
        enter_key = Configuration.read_config_value("KEYS", "STOP_KEY", "ClientConfiguration.conf")
        plus_key = Configuration.read_config_value("KEYS", "UP_KEY", "ClientConfiguration.conf")
        minus_key = Configuration.read_config_value("KEYS", "DOWN_KEY", "ClientConfiguration.conf")

        while is_calibrating_step_1 or is_calibrating_step_2:
            calibration_request = ""
            try:
                pressed_key = ord(getch())
            except UnicodeDecodeError:
                continue

            if pressed_key == enter_key:
                if is_calibrating_step_1:
                    is_calibrating_step_1 = False
                else:
                    is_calibrating_step_2 = False
                calibration_request = Configuration.read_config_value("CLIENT_COMMANDS", "CALIBRATE_STOP_COMMAND",
                                                                      "ClientConfiguration.conf")
            elif pressed_key == plus_key:
                calibration_request = Configuration.read_config_value("CLIENT_COMMANDS", "CALIBRATE_UP_COMMAND",
                                                                      "ClientConfiguration.conf")
            elif pressed_key == minus_key:
                calibration_request = Configuration.read_config_value("CLIENT_COMMANDS", "CALIBRATE_DOWN_COMMAND",
                                                                      "ClientConfiguration.conf")
            send_server_request(calibration_request)
        return True
    return False


""" MAIN PROGRAM """

# Define the keypress method for Windows...
if os.name == 'nt':
    import msvcrt

    def getch():
        return msvcrt.getch().decode()
# or the keypress method for Unix...
else:
    import tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    tty.setraw(sys.stdin.fileno())

    def getch():
        return sys.stdin.read(1)


# Get the request
arguments = sys.argv[1:]
request = ""
for argument in arguments:
    request += argument + " "
request = request.strip()

# Empty request = show commands (help command)
if len(request) == 0:
    help_command = Configuration.read_config_value("CLIENT_COMMANDS", "HELP_COMMAND", "ClientConfiguration.conf")
    request = help_command

# Execute calibration if it is possible
calibration_process = check_calibration_command(request.split(" ")[0])

# If calibration was not executed, execute normal command
if not calibration_process:
    sock = send_server_request(request)
    wait_server_response(sock)