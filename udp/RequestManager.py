import time
from collections import deque
from Config.Configuration import Configuration
from EngineRequests.RequestsList import RequestsList
from Engine.EngineStatus import EngineStatus
from Engine.EngineAction import EngineAction
from EngineRequests.CalibrationActions.DecrementRequest import DecrementRequest
from EngineRequests.CalibrationActions.IncrementRequest import IncrementRequest
from EngineRequests.CalibrationActions.StopRequest import StopRequest


class RequestManager:
    def __init__(self, sock):
        self.requests_queue = deque()
        self.sock = sock
        self.socket_busy = False
        self.engine_status = EngineStatus()
        self.engine = EngineAction()

    @staticmethod
    def initialize_actions():
        RequestsList.init_actions()

    """
    Queue Management
    """

    def add_request(self, request, client):
        self.requests_queue.append(request)
        self.requests_queue.append(client)

    def get_current_queue(self):
        return self.requests_queue

    def get_next_action(self):
        request = self.requests_queue.popleft()
        client = self.requests_queue.popleft()
        return request, client

    """
    Main queue actions: wait for elements and run elements from queue
    """

    def wait_for_requests(self):
        while True:
            try:
                print('\nWaiting incoming requests...')
                data, client = self.sock.recvfrom(4096)

                print('Received {} Bytes from Client ({})'.format(
                    len(data), client)
                )

                # Data received
                data = data.decode('utf-8')
                print('Received request: ', data)

                # There is some data
                if data:
                    # Process the request
                    # - Calibration Commands: stop, increment, decrement, ...
                    # - Special command: help, exit,...
                    # - Normal command: connection, rotate,...
                    self.process_data(data, client)

                # There is no data (Empty request, just send 'enter')
                else:
                    message = "Error. No command sent."
                    self.sock.sendto(message.encode(), client)
                    print(message)

            except ConnectionResetError:
                while self.socket_busy:
                    time.sleep(2)
                self.wait_for_requests()

    def run_request(self):
        if len(self.get_current_queue()) > 0:
            request, client = self.get_next_action()
            if request:
                # Split data for commands
                split_data = request.split(" ")
                command = split_data[0]
                parameters = []
                for param in split_data[1:]:
                    parameters.append(param)

                selected_key = None
                selected_action = None
                for key, action in RequestsList.get_actions():
                    if command == str(key):
                        print("Request: {} => {} {}\n".format(action.get_name(), command, parameters))
                        selected_key = key
                        selected_action = action
                        break

                # Socket busy sending response
                self.socket_busy = True

                # key found
                if selected_key is not None:
                    sent = self.sock.sendto(selected_action.run(parameters).encode(), client)
                    print('Return {} Byte-data to Client {}'
                          .format(sent, client))
                # Invalid option (NOT FOUND IN API METHOD)
                else:
                    message = "Error. Invalid command."
                    self.sock.sendto(message.encode(), client)
                    print(message)

                # Socket is already free
                self.socket_busy = False

            else:
                message = "Error. No command sent."
                self.sock.sendto(message.encode(), client)
                print(message)

    def process_data(self, data, client):
        if self.engine_status.is_in_calibration():
            if data == Configuration.read_config_value("COMMANDS", "CALIBRATE_STOP_COMMAND",
                                                       "Configuration.conf"):
                response = StopRequest().run([])
                sent = self.sock.sendto(response.encode(), client)
                print('Return {} Byte-data to Client {}'.format(sent, client))

            # Commands only for manual calibration, not automatic calibration
            elif self.engine_status.is_in_manual_calibration():
                if data == Configuration.read_config_value("COMMANDS", "CALIBRATE_UP_COMMAND",
                                                           "Configuration.conf"):
                    response = IncrementRequest().run([])
                    sent = self.sock.sendto(response.encode(), client)
                    print('Return {} Byte-data to Client {}'.format(sent, client))

                elif data == Configuration.read_config_value("COMMANDS", "CALIBRATE_DOWN_COMMAND",
                                                             "Configuration.conf"):
                    response = DecrementRequest().run([])
                    sent = self.sock.sendto(response.encode(), client)
                    print('Return {} Byte-data to Client {}'.format(sent, client))
        else:
            # Exit server
            if data == Configuration.read_config_value("COMMANDS", "EXIT_COMMAND", "Configuration.conf"):
                exit(0)
            # Send command help to client
            elif data == Configuration.read_config_value("COMMANDS", "HELP_COMMAND", "Configuration.conf"):
                separator = "\n--------------------------------------------------------\n"
                action_list = separator + "COMMANDS" + separator
                for key, action in RequestsList.get_actions():
                    action_list += "{} => {} {},\n".format(action.get_name(), key, action.get_params_description())
                sent = self.sock.sendto((action_list[:-2] + separator).encode(), client)
                print('Return {} Byte-data to Client {}'.format(sent, client))
            else:
                print('Request added.')
                self.add_request(data, client)
