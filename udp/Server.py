import time
import socket
from threading import Thread
from Config.Configuration import Configuration
from udp.RequestManager import RequestManager

# Create a udp socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Create Request Manager and initialize available actions in server
r_manager = RequestManager(sock)
r_manager.initialize_actions()

# Bind the socket to the port
ip_address = Configuration.read_config_value("SETTINGS", "SERVER_IP", "Configuration.conf")
port = Configuration.read_config_value("SETTINGS", "SERVER_PORT", "Configuration.conf")
server_address = (ip_address, port)
print('\nStarting server on: {}:{}'.format(*server_address))
sock.bind(server_address)

# Wait for request thread
wait_thread = Thread(target=r_manager.wait_for_requests, args=())
wait_thread.start()

time_between_commands = int(Configuration.read_config_value("VALUES", "TIME_BETWEEN_COMMANDS", "Configuration.conf"))
while True:
    time.sleep(time_between_commands)
    r_manager.run_request()
