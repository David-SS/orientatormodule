import os
import sys
import socket
from threading import Thread
from Config.Configuration import Configuration


""" FUNCTIONS """

def send_server_request(client_request):
    # Create a udp socket
    ip_address = Configuration.read_config_value("SETTINGS", "SERVER_IP", "ClientConfiguration.conf")
    port = Configuration.read_config_value("SETTINGS", "SERVER_PORT", "ClientConfiguration.conf")
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = (ip_address, port)

    # Send request // Todo: Removed exception... think about that:
    print('Sending request [ {!r} ]'.format(client_request))
    sent = sock.sendto(client_request.encode(), server_address)
    return sock


def wait_server_response(sock):
    print('Waiting for response...')
    try:
        data, server = sock.recvfrom(4096)
        data = data.decode('utf-8')
        print("Received data: {}".format(data))

    except ConnectionResetError:
        print("There were problems in the server side, check it and try again...")

    print('Closing socket')
    sock.close()


""" MAIN PROGRAM """

# Define the keypress method for Windows...
if os.name == 'nt':
    import msvcrt

    def getch():
        return msvcrt.getch().decode()
# or the keypress method for Unix...
else:
    import tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    tty.setraw(sys.stdin.fileno())

    def getch():
        return sys.stdin.read(1)

# Define used keys for keypress event
enter_key = Configuration.read_config_value("KEYS", "STOP_KEY", "ClientConfiguration.conf")
plus_key = Configuration.read_config_value("KEYS", "UP_KEY", "ClientConfiguration.conf")
minus_key = Configuration.read_config_value("KEYS", "DOWN_KEY", "ClientConfiguration.conf")


# Exit and Help keyword and first message to user
exit_command = Configuration.read_config_value("CLIENT_COMMANDS", "EXIT_COMMAND", "ClientConfiguration.conf")
help_command = Configuration.read_config_value("CLIENT_COMMANDS", "HELP_COMMAND", "ClientConfiguration.conf")
print("Type '{}' to exit or '{}' if you want to get the list of commands".format(exit_command, help_command))

# Initial calibrating state
is_calibrating_step_1 = False
is_calibrating_step_2 = False
start_auto_calibrate_command = Configuration.read_config_value("CLIENT_COMMANDS", "AUTOMATIC_CALIBRATE_COMMAND",
                                                               "ClientConfiguration.conf")
start_manual_calibrate_command = Configuration.read_config_value("CLIENT_COMMANDS", "MANUAL_CALIBRATE_COMMAND",
                                                                 "ClientConfiguration.conf")

while True:
    if is_calibrating_step_1 or is_calibrating_step_2:
        request = ""
        try:
            pressed_key = ord(getch())
        except UnicodeDecodeError:
            continue

        if pressed_key == enter_key:
            if is_calibrating_step_1:
                is_calibrating_step_1 = False
            else:
                is_calibrating_step_2 = False
            request = Configuration.read_config_value("CLIENT_COMMANDS", "CALIBRATE_STOP_COMMAND",
                                                      "ClientConfiguration.conf")
        elif pressed_key == plus_key:
            request = Configuration.read_config_value("CLIENT_COMMANDS", "CALIBRATE_UP_COMMAND",
                                                      "ClientConfiguration.conf")
        elif pressed_key == minus_key:
            request = Configuration.read_config_value("CLIENT_COMMANDS", "CALIBRATE_DOWN_COMMAND",
                                                      "ClientConfiguration.conf")
        send_server_request(request)

    else:
        request = str(input()).strip()
        if not is_calibrating_step_2:
            if request == start_manual_calibrate_command or request == start_auto_calibrate_command:
                is_calibrating_step_1 = True
                is_calibrating_step_2 = True

        sock = send_server_request(request)

        # Check for exit command (Close client)
        if request == exit_command:
            break

        # Receive response
        wait_for_response_thread = Thread(target=wait_server_response, args=(sock,))
        wait_for_response_thread.start()
