
class MathsTools:

    # y = mx + n
    def __init__(self, min_degree=0, max_degree=360, min_tachometer=0, max_tachometer=4096):
        # scale
        self.min_degree = min_degree
        self.max_degree = max_degree
        self.min_tachometer = min_tachometer
        self.max_tachometer = max_tachometer

        # lineal conversion
        self.m_degree_to_tachometer = 0.0
        self.m_tachometer_to_degree = 0.0
        self.n_degree_to_tachometer = 0.0
        self.n_tachometer_to_degree = 0.0

    # y = mx + n
    def calculate_m_and_n(self):
        if (self.max_degree - self.min_degree) <= 0:
            return False
        if (self.max_tachometer - self.min_tachometer) <= 0:
            return False

        self.m_degree_to_tachometer = (self.max_tachometer - self.min_tachometer) / (self.max_degree - self.min_degree)
        self.n_degree_to_tachometer = self.min_tachometer - self.m_degree_to_tachometer * self.min_degree

        self.m_tachometer_to_degree = (self.max_degree - self.min_degree) / (self.max_tachometer - self.min_tachometer)
        self.n_tachometer_to_degree = self.min_degree - self.m_tachometer_to_degree * self.min_tachometer
        return True

    def degree_to_tachometer(self, degrees):
        return round(self.m_degree_to_tachometer * degrees + self.n_degree_to_tachometer)

    def tachometer_to_degree(self, tachometer):
        return round(self.m_tachometer_to_degree * tachometer + self.n_tachometer_to_degree, 2)

    def get_tachometer_value(self, degrees):
        # Always true for default values. If there is a user interaction,
        # we should check that calculate_m_and_n is true before continue
        self.calculate_m_and_n()
        return self.degree_to_tachometer(degrees)

    def get_degree_value(self, tachometer):
        # Always true for default values. If there is a user interaction,
        # we should check that calculate_m_and_n is true before continue
        self.calculate_m_and_n()
        return self.tachometer_to_degree(tachometer)

    @staticmethod
    # Both params between 0-360 degrees
    def get_optimal_rotation(current_degrees, next_degrees):
        # alternative range
        above_extended_next_degrees = next_degrees + 360
        below_extended_next_degrees = next_degrees - 360

        # calculate differences (degrees to rotate for each option)
        next_degrees_dif = abs(current_degrees - next_degrees)
        above_extended_next_degrees_dif = abs(current_degrees - above_extended_next_degrees)
        below_extended_next_degrees_dif = abs(current_degrees - below_extended_next_degrees)

        # return the shortest value (less degrees to rotate)
        optimal_value = next_degrees
        if (above_extended_next_degrees_dif < next_degrees_dif
                and above_extended_next_degrees_dif < below_extended_next_degrees_dif):
            optimal_value = above_extended_next_degrees
        if (below_extended_next_degrees_dif < next_degrees_dif
                and below_extended_next_degrees_dif < above_extended_next_degrees_dif):
            optimal_value = below_extended_next_degrees
        return round(optimal_value, 2)
