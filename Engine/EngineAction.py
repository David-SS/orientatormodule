from src.dynamixel_sdk import *
from threading import Semaphore
from Engine.EngineConnection import EngineConnection
from Config.Configuration import Configuration


class EngineAction:
    # Singleton instance of this class
    __instance = None

    def __new__(cls, *args, **kwargs):
        # When call EngineStatus() we assign to "self" the new object or...
        if cls.__instance is None:
            cls.__instance = super(EngineAction, cls).__new__(cls)
            cls.__instance.__initialized = False
        # the existing one (Singleton)
        return cls.__instance

    def __init__(self):
        # Only set initial values when instantiated first time
        if self.__initialized:
            return
        self.__initialized = True
        self.__serial_connection = EngineConnection()
        self.__engine_semaphore = Semaphore(1)

    def __str__(self):
        return str(id(self)) + " - " + str(self.__dict__)

    """
    BYTES LEVEL ACTIONS
    """

    def read_one_byte(self, flag):
        if self.__serial_connection.is_connected():
            with self.__engine_semaphore:
                engine_port_handler = self.__serial_connection.get_port_handler()
                engine_packet_handler = self.__serial_connection.get_packet_handler()
                read_value, dxl_comm_result, dxl_error = engine_packet_handler.read1ByteTxRx(
                    engine_port_handler,
                    self.__serial_connection.get_id(),
                    flag
                )
                # If any error appears
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % engine_packet_handler.getTxRxResult(dxl_comm_result))
                    return None
                elif dxl_error != 0:
                    print("%s" % engine_packet_handler.getRxPacketError(dxl_error))
                    return None
                # Success if not
                return read_value

    def read_two_bytes(self, flag):
        if self.__serial_connection.is_connected():
            with self.__engine_semaphore:
                engine_port_handler = self.__serial_connection.get_port_handler()
                engine_packet_handler = self.__serial_connection.get_packet_handler()
                read_value, dxl_comm_result, dxl_error = engine_packet_handler.read2ByteTxRx(
                    engine_port_handler,
                    self.__serial_connection.get_id(),
                    flag
                )
                # If any error appears
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % engine_packet_handler.getTxRxResult(dxl_comm_result))
                    return None
                elif dxl_error != 0:
                    print("%s" % engine_packet_handler.getRxPacketError(dxl_error))
                    return None
                # Success if not
                return read_value

    def read_four_bytes(self, flag):
        if self.__serial_connection.is_connected():
            with self.__engine_semaphore:
                engine_port_handler = self.__serial_connection.get_port_handler()
                engine_packet_handler = self.__serial_connection.get_packet_handler()
                read_value, dxl_comm_result, dxl_error = engine_packet_handler.read4ByteTxRx(
                    engine_port_handler,
                    self.__serial_connection.get_id(),
                    flag
                )
                # If any error appears
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % engine_packet_handler.getTxRxResult(dxl_comm_result))
                    return None
                elif dxl_error != 0:
                    print("%s" % engine_packet_handler.getRxPacketError(dxl_error))
                    return None
                # Success if not
                return read_value

    def write_one_byte(self, flag, value):
        if self.__serial_connection.is_connected():
            with self.__engine_semaphore:
                engine_port_handler = self.__serial_connection.get_port_handler()
                engine_packet_handler = self.__serial_connection.get_packet_handler()
                dxl_comm_result, dxl_error = engine_packet_handler.write1ByteTxRx(
                    engine_port_handler,
                    self.__serial_connection.get_id(),
                    flag,
                    value
                )
                # If any error appears
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % engine_packet_handler.getTxRxResult(dxl_comm_result))
                    return False
                elif dxl_error != 0:
                    print("%s" % engine_packet_handler.getRxPacketError(dxl_error))
                    return False
                # Success if not
                return True

    def write_two_bytes(self, flag, value):
        if self.__serial_connection.is_connected():
            with self.__engine_semaphore:
                engine_port_handler = self.__serial_connection.get_port_handler()
                engine_packet_handler = self.__serial_connection.get_packet_handler()
                dxl_comm_result, dxl_error = engine_packet_handler.write2ByteTxRx(
                    engine_port_handler,
                    self.__serial_connection.get_id(),
                    flag,
                    value
                )
                # If any error appears
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % engine_packet_handler.getTxRxResult(dxl_comm_result))
                    return False
                elif dxl_error != 0:
                    print("%s" % engine_packet_handler.getRxPacketError(dxl_error))
                    return False
                # Success if not
                return True

    def write_four_bytes(self, flag, value):
        if self.__serial_connection.is_connected():
            with self.__engine_semaphore:
                engine_port_handler = self.__serial_connection.get_port_handler()
                engine_packet_handler = self.__serial_connection.get_packet_handler()
                dxl_comm_result, dxl_error = engine_packet_handler.write4ByteTxRx(
                    engine_port_handler,
                    self.__serial_connection.get_id(),
                    flag,
                    value
                )
                # If any error appears
                if dxl_comm_result != COMM_SUCCESS:
                    print("%s" % engine_packet_handler.getTxRxResult(dxl_comm_result))
                    return False
                elif dxl_error != 0:
                    print("%s" % engine_packet_handler.getRxPacketError(dxl_error))
                    return False
                # Success if not
                return True

    """
    USER LEVEL ACTIONS (THAT READ FROM ENGINE OR WRITE IN ENGINE)
    """

    def is_engine_moving(self):
        if self.__serial_connection.is_connected():
            return self.read_one_byte(
                Configuration.read_config_value('ADDRESSES', 'ADDR_MOVING', "Configuration.conf")
            )

    def get_operating_mode(self):
        if self.__serial_connection.is_connected():
            return self.read_one_byte(
                Configuration.read_config_value('ADDRESSES', 'ADDR_OPERATING_MODE', "Configuration.conf")
            )

    def get_absolute_present_position(self):
        if self.__serial_connection.is_connected():
            return self.read_four_bytes(
                Configuration.read_config_value('ADDRESSES', 'ADDR_PRESENT_POSITION', "Configuration.conf")
            )

    def get_goal_velocity(self):
        if self.__serial_connection.is_connected():
            return self.read_four_bytes(
                Configuration.read_config_value('ADDRESSES', 'ADDR_GOAL_VELOCITY', "Configuration.conf"),
            )

    def enable_torque(self):
        if self.__serial_connection.is_connected():
            return self.write_one_byte(
                Configuration.read_config_value('ADDRESSES', 'ADDR_TORQUE_ENABLE', "Configuration.conf"),
                Configuration.read_config_value('VALUES', 'TORQUE_ENABLE', "Configuration.conf")
            )

    def disable_torque(self):
        if self.__serial_connection.is_connected():
            return self.write_one_byte(
                Configuration.read_config_value('ADDRESSES', 'ADDR_TORQUE_ENABLE', "Configuration.conf"),
                Configuration.read_config_value('VALUES', 'TORQUE_DISABLE', "Configuration.conf")
            )

    def reset_present_position(self):
        if self.__serial_connection.is_connected():
            # need this doc official operation to reset the present position value
            current_state = self.read_one_byte(Configuration.read_config_value('ADDRESSES', 'ADDR_OPERATING_MODE',
                                                                               "Configuration.conf"))
            self.disable_torque()
            self.write_one_byte(
                Configuration.read_config_value('ADDRESSES', 'ADDR_OPERATING_MODE', "Configuration.conf"),
                Configuration.read_config_value('OPERATING_MODES', 'POSITION_CM', "Configuration.conf")
            )
            self.enable_torque()
            # back to initial state
            self.disable_torque()
            self.write_one_byte(
                Configuration.read_config_value('ADDRESSES', 'ADDR_OPERATING_MODE', "Configuration.conf"),
                current_state
            )

    def set_default_operating_mode(self):
        if self.__serial_connection.is_connected():
            return self.write_one_byte(
                Configuration.read_config_value('ADDRESSES', 'ADDR_OPERATING_MODE', "Configuration.conf"),
                Configuration.read_config_value('OPERATING_MODES', 'EXTENDED_POSITION_CM', "Configuration.conf")
            )

    def set_operating_mode(self, mode):
        if self.__serial_connection.is_connected():
            modes = Configuration.read_config_section("OPERATING_MODES", "Configuration.conf")
            for key in modes:
                if key == mode:
                    return self.write_one_byte(
                        Configuration.read_config_value('ADDRESSES', 'ADDR_OPERATING_MODE', "Configuration.conf"),
                        modes[key]
                    )
            # Passed mode is not valid
            return None

    def set_goal_position(self, value):
        if self.__serial_connection.is_connected():
            return self.write_four_bytes(
                Configuration.read_config_value('ADDRESSES', 'ADDR_GOAL_POSITION', "Configuration.conf"),
                value
            )

    def set_default_goal_velocity(self):
        if self.__serial_connection.is_connected():
            return self.write_four_bytes(
                Configuration.read_config_value('ADDRESSES', 'ADDR_GOAL_VELOCITY', "Configuration.conf"),
                Configuration.read_config_value('VALUES', 'DEFAULT_VELOCITY', "Configuration.conf")
            )

    def set_goal_velocity(self, value):
        if self.__serial_connection.is_connected():
            return self.write_four_bytes(
                Configuration.read_config_value('ADDRESSES', 'ADDR_GOAL_VELOCITY', "Configuration.conf"),
                value
            )
