import time
from Config.Configuration import Configuration
from Engine.EngineAction import EngineAction


class EngineStatus:

    # Singleton instance of this class
    __instance = None

    def __new__(cls, *args, **kwargs):
        # When call EngineStatus() we assign to "self" the new object or...
        if cls.__instance is None:
            cls.__instance = super(EngineStatus, cls).__new__(cls)
            cls.__instance.__initialized = False
        # the existing one (Singleton)
        return cls.__instance

    def __init__(self):
        # Only set initial values when instantiated first time
        if self.__initialized:
            return
        self.__initialized = True
        self.__engine = EngineAction()
        self.__general_calibration = False
        self.__precise_calibration = False
        self.__manual_calibration = False

    def __str__(self):
        return str(id(self)) + " - " + str(self.__dict__)

    """ 
        GENERAL STATUS METHODS
    """

    @staticmethod
    def get_origin_degrees():
        return float(Configuration.read_config_value("VALUES", "ORIGIN_DEGREES", "Configuration.conf"))

    def wait_until_stop(self, period=0.5, timeout=2500):
        must_end = time.time() + timeout
        while time.time() < must_end:
            time.sleep(period)
            if not self.__engine.is_engine_moving():
                return True
        return False

    def switch_rotation(self, period=0.5, reaction_time=2):
        time.sleep(reaction_time)
        current_velocity = Configuration.read_config_value('VALUES', 'DEFAULT_VELOCITY', "Configuration.conf")
        while self.__engine.is_engine_moving():
            time.sleep(period)
            current_position = self.__engine.get_absolute_present_position()
            if current_position < 0 or current_position > 4096:
                current_velocity *= -1
                self.__engine.set_goal_velocity(current_velocity)
                time.sleep(reaction_time)
        return True

    """ 
        CALIBRATION RELATED METHODS
    """

    def is_in_calibration(self):
        # Never [general calibration = True] and [precise calibration = False]
        return self.__precise_calibration

    def is_in_manual_calibration(self):
        return self.__manual_calibration

    def enable_calibration_status(self):
        self.__general_calibration = True
        self.__precise_calibration = True

    def manual_calibration_on(self):
        self.__manual_calibration = True

    def complete_calibration_step(self):
        if self.__general_calibration:
            self.__general_calibration = False
        else:
            self.__precise_calibration = False
            self.__manual_calibration = False
