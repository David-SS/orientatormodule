import re
import serial.tools.list_ports
from src.dynamixel_sdk import *
from Config.Configuration import Configuration


class EngineConnection:

    # Singleton instance of this class
    __instance = None
    __connected = False

    def __new__(cls, *args, **kwargs):
        # When call EngineStatus() we assign to "self" the new object or...
        if cls.__instance is None:
            cls.__instance = super(EngineConnection, cls).__new__(cls)
            cls.__instance.__initialized = False
        # the existing one (Singleton)
        return cls.__instance

    def __init__(self):
        # Only set initial values when instantiated first time
        if self.__initialized:
            return
        self.__initialized = True
        self.__default_port_keyword = Configuration.read_config_value("SETTINGS", "PORT_KEYWORD", "Configuration.conf")
        self.__port_keyword = self.__default_port_keyword
        self.__engine_port = 'None'  # No connection yet
        self.__engine_port_handler = None  # No connection yet
        self.__engine_packet_handler = None  # No connection yet

    def __str__(self):
        return str(id(self)) + " - " + str(self.__dict__)

    """ 
        CONNECTION METHODS
    """

    @staticmethod
    def get_id():
        return Configuration.read_config_value("SETTINGS", "DXL_ID", "Configuration.conf")

    def get_port_handler(self):
        return self.__engine_port_handler

    def get_packet_handler(self):
        return self.__engine_packet_handler

    def get_port(self):
        if self.__engine_port.__eq__('None'):
            self.find_port()
        return self.__engine_port

    def find_port(self):
        available_ports = serial.tools.list_ports.comports()
        num_of_ports = len(available_ports)
        for i in range(0, num_of_ports):
            current_port = str(available_ports[i])
            if self.__port_keyword in current_port:
                self.__engine_port = re.search('COM[0-9]{1,2}', current_port).group()
                break

    def is_connected(self):
        return self.__connected

    def set_port_keyword(self, keyword):
        self.__port_keyword = keyword

    def set_default_port_keyword(self):
        self.__port_keyword = self.__default_port_keyword

    def clean_connection_data(self):
        self.__engine_port = 'None'
        self.__engine_port_handler = None
        self.__engine_packet_handler = None

    def refresh_connection_data(self):
        self.__engine_port = 'None'
        self.__engine_port_handler = PortHandler(self.get_port())
        protocol_version = Configuration.read_config_value("SETTINGS", "PROTOCOL_VERSION", "Configuration.conf")
        self.__engine_packet_handler = PacketHandler(protocol_version)

    def connect_engine(self):
        # Refresh port and packet handlers to try a new connection
        self.refresh_connection_data()

        # Open port and set port baudrate
        try:
            self.__engine_port_handler.openPort()
            print("Succeeded to open the port")
            baudrate = Configuration.read_config_value("SETTINGS", "BAUDRATE", "Configuration.conf")
            self.__engine_port_handler.setBaudRate(baudrate)
            print("Succeeded to change the baudrate")
            self.__connected = True
            print("Succeeded to connect")
            return True
        except Exception as ex:
            print("Failed to open the port and set baudrate")
            print(ex)
            return False

    def disconnect_engine(self):
        # Disconnect if it was connected and clean all variables/handlers
        try:
            self.__engine_port_handler.closePort()
            self.clean_connection_data()
            self.__connected = False
            print("Succeeded to disconnect")
            return True
        except Exception as ex:
            print("Failed to disconnect")
            print(ex)
            return False
