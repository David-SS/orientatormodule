import configparser
import os
import time


class Configuration:
    @classmethod
    def __get_conf(cls, config_file):
        configuration = configparser.ConfigParser()
        configuration.optionxform = lambda option: option   # To preserve case
        current_dir = os.path.dirname(os.path.abspath(__file__))
        configuration.read(os.path.join(current_dir, config_file))
        return configuration, current_dir

    @classmethod
    def write_origin_degrees(cls, value):
        configuration, current_dir = cls.__get_conf("Configuration.conf")
        configuration.set("VALUES", "ORIGIN_DEGREES", str(value))
        with open(os.path.join(current_dir, 'Configuration.conf'), 'w') as configfile:
            configuration.write(configfile)

    @classmethod
    def read_config_section(cls, section, config_file):
        try:
            configuration, current_dir = cls.__get_conf(config_file)
            section_values = configuration.items(section)
            formatted_section_values = {}
            for key, value in section_values:
                try:
                    # Cast to int if there is no '.' and to float if not
                    formatted_section_values[key.upper()] = float(value) if '.' in value else int(value)
                except ValueError:
                    # If it was not possible, then it was a String
                    formatted_section_values[key.upper()] = value
            return formatted_section_values
        # If fails, just wait and try again (maybe file was externally modified)
        except configparser.NoSectionError:
            time.sleep(1)
            cls.read_config_section(section, config_file)

    @classmethod
    def read_config_value(cls, section, key, config_file):
        return cls.read_config_section(section, config_file)[key.upper()]

