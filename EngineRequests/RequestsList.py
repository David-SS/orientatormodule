from Config.Configuration import Configuration
from EngineRequests.Requests.ConnectionRequest import ConnectionRequest
from EngineRequests.Requests.DisconnectionRequest import DisconnectionRequest
from EngineRequests.Requests.GetEnginePositionRequest import GetEnginePositionRequest
from EngineRequests.Requests.RandomRotation import RandomRotationRequest
from EngineRequests.Requests.RotationRequest import RotationRequest
from EngineRequests.CalibrationRequests.AutomaticCalibrationRequest import AutomaticCalibrationRequest
from EngineRequests.CalibrationRequests.ManualCalibrationRequest import ManualCalibrationRequest
from EngineRequests.CalibrationRequests.SetCalibrationRequest import SetCalibrationRequest


class RequestsList:
    __actions = {}

    @classmethod
    def init_actions(cls):
        commands = Configuration.read_config_section("COMMANDS", "Configuration.conf")
        cls.__actions[commands['CONNECT_COMMAND']] = ConnectionRequest()
        cls.__actions[commands['DISCONNECT_COMMAND']] = DisconnectionRequest()
        cls.__actions[commands['GET_DEGREES_COMMAND']] = GetEnginePositionRequest()
        cls.__actions[commands['RANDOM_ROTATE_COMMAND']] = RandomRotationRequest()
        cls.__actions[commands['ROTATE_COMMAND']] = RotationRequest()
        cls.__actions[commands['SET_CALIBRATION_VALUE_COMMAND']] = SetCalibrationRequest()
        cls.__actions[commands['AUTOMATIC_CALIBRATE_COMMAND']] = AutomaticCalibrationRequest()
        cls.__actions[commands['MANUAL_CALIBRATE_COMMAND']] = ManualCalibrationRequest()

    @classmethod
    def get_actions(cls):
        return cls.__actions.items()

    @classmethod
    def get_key(cls, action_value):
        # for a, b in dictionary.iteritems():  <-- Python 2.x
        for key, action in cls.__actions.items():
            if action.get_name() == action_value:
                return key
            else:
                continue
        return None

