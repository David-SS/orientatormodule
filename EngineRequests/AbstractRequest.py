from abc import ABC, abstractmethod


class AbstractRequest(ABC):
    # Dear programmer: implement this in a subclass OR YOU'LL BE SORRY!
    name = NotImplemented
    params_description = NotImplemented

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)

        # If the new class did not redefine name, fail *hard*.
        if cls.name is NotImplemented or cls.params_description is NotImplemented:
            # Choose your favorite exception.
            raise NotImplementedError('Please, define the attribute "name"')

    @classmethod
    def get_name(cls): return cls.name

    @classmethod
    def get_params_description(cls): return cls.params_description

    @abstractmethod
    def run(self, params): pass
