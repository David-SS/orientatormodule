from Config.Configuration import Configuration
from Engine.EngineConnection import EngineConnection
from Engine.EngineStatus import EngineStatus
from Engine.EngineAction import EngineAction
from Engine.MathsTools import MathsTools
from EngineRequests.AbstractRequest import AbstractRequest


class DecrementRequest(AbstractRequest):
    name = "Decrement degrees"
    params_description = "[]"

    def run(self, params):
        engine_connection = EngineConnection()
        # Do the action
        if engine_connection.is_connected():
            # Get the increment value in tachometers
            increment = Configuration.read_config_value("VALUES", "CALIBRATION_PRECISION", "Configuration.conf")
            increment_tachometer = MathsTools().get_tachometer_value(increment)

            # Execute the special calibration action: increment
            engine_status = EngineStatus()
            engine = EngineAction()
            engine.reset_present_position()
            engine.set_default_operating_mode()
            engine.enable_torque()
            current_position = engine.get_absolute_present_position()
            engine.set_goal_position(current_position - increment_tachometer)
            engine_status.wait_until_stop(1)

            return "Executed " + self.name

        # Action cancelled
        else:
            print("Engine is not connected")
            return "Connect engine before executing " + self.name

