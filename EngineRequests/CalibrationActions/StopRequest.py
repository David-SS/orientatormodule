from Engine.EngineConnection import EngineConnection
from Engine.EngineStatus import EngineStatus
from Engine.EngineAction import EngineAction
from EngineRequests.AbstractRequest import AbstractRequest


class StopRequest(AbstractRequest):
    name = "Stop in calibration"
    params_description = "[]"

    def run(self, params):
        engine_connection = EngineConnection()
        # Do the action
        if engine_connection.is_connected():
            # Execute the special calibration action: stop
            engine_status = EngineStatus()
            engine = EngineAction()
            engine_status.complete_calibration_step()
            engine.disable_torque()
            engine.reset_present_position()
            engine.set_default_operating_mode()

            return "Stopped engine in Calibration Mode."

        # Action cancelled
        else:
            print("Engine is not connected")
            return "Connect engine before executing " + self.name

