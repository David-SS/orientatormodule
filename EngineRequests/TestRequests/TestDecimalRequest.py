from Engine.EngineConnection import EngineConnection
from Engine.EngineStatus import EngineStatus
from Engine.EngineAction import EngineAction
from Engine.MathsTools import MathsTools
from EngineRequests.AbstractRequest import AbstractRequest


class TestDecimalRequest(AbstractRequest):
    name = "Test Decimal"
    params_description = "[]"

    def run(self, params):
        engine_connection = EngineConnection()
        # Do the action
        if engine_connection.is_connected():
            engine_rotations = "/"
            rotation = 0.0
            increment = 0.5
            final_degrees = 5.0
            engine_status = EngineStatus()
            engine = EngineAction()
            engine.reset_present_position()
            engine.set_default_operating_mode()
            engine.enable_torque()
            while rotation <= final_degrees:
                engine.set_goal_position(MathsTools().get_tachometer_value(rotation))
                engine_status.wait_until_stop()
                engine_rotations += str(rotation) + '=' + str(engine.get_absolute_present_position()) + '/'
                rotation += increment
            engine.disable_torque()
            return "Executed " + self.name + " || Result: " + engine_rotations
        # Action cancelled
        else:
            print("Engine is not connected")
            return "Connect engine before executing " + self.name
