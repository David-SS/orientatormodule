from Engine.EngineConnection import EngineConnection
from Engine.EngineStatus import EngineStatus
from Engine.EngineAction import EngineAction
from Engine.MathsTools import MathsTools
from EngineRequests.AbstractRequest import AbstractRequest


class GetEnginePositionRequest(AbstractRequest):
    name = "Get Engine Position"
    params_description = "[]"

    def run(self, params):
        # Do the action
        engine_connection = EngineConnection()
        if engine_connection.is_connected():
            engine_status = EngineStatus()
            engine = EngineAction()
            engine.reset_present_position()
            degrees_actions = MathsTools()

            abs_tachometers = engine.get_absolute_present_position()
            abs_degrees = degrees_actions.get_degree_value(abs_tachometers)

            # rare operations to avoid negative degrees errors:
            rel_degrees = (abs_degrees + 360 - engine_status.get_origin_degrees()) % 360
            rel_tachometers = degrees_actions.get_tachometer_value(rel_degrees)

            return (
                    "Executed " + self.name +
                    " :: Virtual Degrees = " + str(round(rel_degrees, 2)) +
                    " [Absolute = " + str(round(abs_degrees, 2)) + "]"
                    " || Virtual Tachometer = " + str(rel_tachometers) +
                    " [Absolute = " + str(abs_tachometers) + "]"
            )
        # Action cancelled
        else:
            print("Engine is not connected")
            return "Connect engine before executing " + self.name
