from Engine.EngineConnection import EngineConnection
from EngineRequests.AbstractRequest import AbstractRequest


class ConnectionRequest(AbstractRequest):
    name = "Connection"
    params_description = "[port_string]"

    def run(self, params):
        engine_connection = EngineConnection()
        if not engine_connection.is_connected():
            # Check for params to setup this request
            if len(params) == 0:
                engine_connection.set_default_port_keyword()
            else:
                engine_connection.set_port_keyword(params[0])

            result = engine_connection.connect_engine()
            if result:
                return "Executed " + self.name
            else:
                return "Connection failed."
        else:
            print("Engine is actually connected.")
            return "Engine is actually connected."
