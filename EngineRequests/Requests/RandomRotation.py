import random
import time
from Config.Configuration import Configuration
from Engine.EngineConnection import EngineConnection
from Engine.EngineStatus import EngineStatus
from Engine.EngineAction import EngineAction
from Engine.MathsTools import MathsTools
from EngineRequests.AbstractRequest import AbstractRequest
from EngineRequests.Requests.RotationRequest import RotationRequest


class RandomRotationRequest(AbstractRequest):
    name = "Random Rotation"
    params_description = "[number_of_rotations]"

    def run(self, params):
        engine_connection = EngineConnection()
        # Do the action
        if engine_connection.is_connected():
            # Needed values to iterate
            executed_rotations = "/"
            random_wait_time = Configuration.read_config_value("VALUES", "TIME_BETWEEN_RANDOM", "Configuration.conf")

            # Check for params to setup this request
            if len(params) == 0:
                iterations = 1
            elif params[0].isnumeric():
                iterations = int(params[0])
            else:
                print("Passed argument is not valid")
                return "Passed argument is not valid"

            # Run the requested action
            while iterations > 0:
                # Get random degrees (between 1-360)
                random_degrees = round(random.uniform(0, 359), 2)
                executed_rotations += str(random_degrees) + "/"
                RotationRequest().run([str(random_degrees)])
                iterations -= 1
                time.sleep(random_wait_time)

            return "Executed " + self.name + ". Random Rotations: " + executed_rotations
        # Action cancelled
        else:
            print("Engine is not connected")
            return "Connect engine before executing " + self.name

