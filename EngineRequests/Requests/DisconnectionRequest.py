from Engine.EngineConnection import EngineConnection
from EngineRequests.AbstractRequest import AbstractRequest


class DisconnectionRequest(AbstractRequest):
    name = "Disconnect"
    params_description = "[]"

    def run(self, params):
        engine_connection = EngineConnection()
        if engine_connection.is_connected():
            result = engine_connection.disconnect_engine()
            if result:
                return "Executed " + self.name
            else:
                return "Disconnection failed."
        else:
            print("Engine is actually not connected.")
            return "Engine is actually not connected."
