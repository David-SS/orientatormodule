from Config.Configuration import Configuration
from Engine.EngineConnection import EngineConnection
from Engine.EngineStatus import EngineStatus
from Engine.EngineAction import EngineAction
from Engine.MathsTools import MathsTools
from EngineRequests.AbstractRequest import AbstractRequest


class RotationRequest(AbstractRequest):
    name = "Rotation"
    params_description = "[float_rotation_degree]"

    def run(self, params):
        engine_connection = EngineConnection()
        # Do the action
        if engine_connection.is_connected():
            # prepare engine
            engine_status = EngineStatus()
            engine = EngineAction()
            engine.reset_present_position()
            engine.set_default_operating_mode()
            engine.enable_torque()

            # Configuration values
            rotation_type = Configuration.read_config_value("VALUES", "OPTIMAL_ROTATION", "Configuration.conf")

            # todo: change to virtual all relative
            # Needed values
            degrees_actions = MathsTools()
            virtual_origin_degrees = engine_status.get_origin_degrees()
            absolute_current_position = degrees_actions.get_degree_value(engine.get_absolute_present_position())

            # Check for params to setup this request
            if len(params) == 0:
                virtual_input_degrees = 0
            elif params[0].isnumeric() or '.' in params[0] or '-' in params[0]:
                try:
                    virtual_input_degrees = (float(params[0]) % 360)
                except ValueError:
                    print("Passed argument is not valid")
                    return "Passed argument is not valid"
            else:
                print("Passed argument is not valid")
                return "Passed argument is not valid"

            # Get the real degrees engine will have to rotate to
            absolute_objective_degrees = virtual_input_degrees + virtual_origin_degrees

            # Optimal rotation => Can rotate to (current position + 179) to the infinite. Absolute value between:
            #   [current_position - 180, current_position + 180]
            if rotation_type == 1:
                # Get the optimal degrees to rotate to
                final_degrees = degrees_actions.get_optimal_rotation(
                    absolute_current_position,
                    absolute_objective_degrees % 360
                )
                tachometer_value = degrees_actions.get_tachometer_value(final_degrees)

            # Not optimal rotation => Absolute value between:
            #   [relative_origin_degrees, relative_origin_degrees + 360]
            else:
                # Calculate virtual current position (in function of origin)
                virtual_current_position = (absolute_current_position - virtual_origin_degrees) % 360
                virtual_current_position = virtual_current_position + virtual_origin_degrees

                # Degrees to rotate from current position
                degrees_to_rotate = absolute_objective_degrees - virtual_current_position
                final_degrees = absolute_current_position + degrees_to_rotate
                tachometer_value = degrees_actions.get_tachometer_value(final_degrees)

            # Rotate calculated degrees
            engine.set_goal_position(tachometer_value)

            # finish engine operations
            engine_status.wait_until_stop(1)
            engine.disable_torque()
            engine.reset_present_position()

            return (
                "Executed " + self.name + ". " +
                "Rotated to " + str(virtual_input_degrees) +
                " (tachometer: " + str(degrees_actions.get_tachometer_value(virtual_input_degrees)) + ")."
            )

        # Action cancelled
        else:
            print("Engine is not connected")
            return "Connect engine before executing " + self.name

