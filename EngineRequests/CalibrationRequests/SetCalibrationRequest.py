from Config.Configuration import Configuration
from EngineRequests.AbstractRequest import AbstractRequest


class SetCalibrationRequest(AbstractRequest):
    name = "Set Calibration"
    params_description = "[absolute_degrees]"

    def run(self, params):
        # Check for params to setup this request
        if len(params) == 0:
            origin_degrees = 0
        else:
            origin_degrees = float(params[0])
        Configuration.write_origin_degrees(origin_degrees)
        return "Executed " + self.name + " (origin degrees: " + str(origin_degrees) + ")"
