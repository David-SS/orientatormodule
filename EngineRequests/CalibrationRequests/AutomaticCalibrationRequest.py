from threading import Thread
from Config.Configuration import Configuration
from Engine.EngineConnection import EngineConnection
from Engine.EngineStatus import EngineStatus
from Engine.EngineAction import EngineAction
from Engine.MathsTools import MathsTools
from EngineRequests.AbstractRequest import AbstractRequest


class AutomaticCalibrationRequest(AbstractRequest):
    name = "Automatic Calibration"
    params_description = "[]"

    def run(self, params):
        engine_connection = EngineConnection()
        # Do the action
        if engine_connection.is_connected():
            # prepare engine (reset engine status to absolute zero)
            engine_status = EngineStatus()
            engine = EngineAction()
            engine.reset_present_position()
            engine.set_default_operating_mode()
            engine.enable_torque()
            engine.set_goal_position(0)
            engine_status.wait_until_stop(1)
            engine.disable_torque()

            # prepare engine for calibration
            engine.reset_present_position()
            engine.set_operating_mode("VELOCITY_CM")
            engine.enable_torque()

            # set the default velocity for calibration
            engine.set_default_goal_velocity()
            engine_status.enable_calibration_status()

            # wait rotating between 0 and 360 degrees (more or less)
            engine_status.switch_rotation(0, 2)
            
            # get current rounded position and get the precise origin degrees in an automatic process
            current_degrees = MathsTools().get_degree_value(engine.get_absolute_present_position())

            # Wait for request thread
            precise_calibration = Thread(
                target=self.precise_calibration, args=(engine, engine_status, current_degrees,)
            )
            precise_calibration.start()

            # Just return without showing result because that causes 'Stop' command does not work...
            # (Program only reads commands like 'Stop' when another command is not running...)
            return "Executed " + self.name + " (general calibration). Ready for precise calibration."

        # Action cancelled
        else:
            print("Engine is not connected")
            return "Connect engine before executing " + self.name

    @staticmethod
    def precise_calibration(engine, engine_status, current_degrees):
        calibration_range = Configuration.read_config_value("VALUES", "CALIBRATION_RANGE", "Configuration.conf")
        increment = Configuration.read_config_value("VALUES", "CALIBRATION_PRECISION", "Configuration.conf")
        time = Configuration.read_config_value("VALUES", "CALIBRATION_TIME_LOOP", "Configuration.conf")

        # Set reference values
        first_value = current_degrees - calibration_range
        final_value = current_degrees + calibration_range

        # 'increment' value normalizes first while iteration
        current_value = first_value - increment

        engine.reset_present_position()
        engine.set_default_operating_mode()
        engine.enable_torque()

        degrees_actions = MathsTools()
        while engine_status.is_in_calibration():
            current_value += increment
            if current_value >= final_value or current_value <= first_value:
                increment = increment * -1
            tachometer_value = degrees_actions.get_tachometer_value(current_value)
            engine.set_goal_position(tachometer_value)
            engine_status.wait_until_stop(time)

        # finish engine operations and write the final value
        engine.disable_torque()
        engine.reset_present_position()
        Configuration.write_origin_degrees(current_value % 360)
        return current_value


