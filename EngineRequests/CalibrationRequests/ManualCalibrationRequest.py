from threading import Thread
from Config.Configuration import Configuration
from Engine.EngineConnection import EngineConnection
from Engine.EngineStatus import EngineStatus
from Engine.EngineAction import EngineAction
from Engine.MathsTools import MathsTools
from EngineRequests.AbstractRequest import AbstractRequest


class ManualCalibrationRequest(AbstractRequest):
    name = "Manual Calibration"
    params_description = "[]"

    def run(self, params):
        engine_connection = EngineConnection()
        # Do the action
        if engine_connection.is_connected():
            # prepare engine (reset engine status to absolute zero)
            engine_status = EngineStatus()
            engine = EngineAction()
            engine.reset_present_position()
            engine.set_default_operating_mode()
            engine.enable_torque()
            engine.set_goal_position(0)
            engine_status.wait_until_stop(1)
            engine.disable_torque()

            # prepare engine for calibration
            engine.reset_present_position()
            engine.set_operating_mode("VELOCITY_CM")
            engine.enable_torque()

            # set the default velocity for calibration
            engine.set_default_goal_velocity()
            engine_status.enable_calibration_status()
            engine_status.manual_calibration_on()

            # finish engine operations
            engine_status.switch_rotation(0, 2)
            engine.disable_torque()
            engine.reset_present_position()

            # Wait for request thread
            precise_calibration = Thread(
                target=self.precise_calibration, args=(engine, engine_status,)
            )
            precise_calibration.start()

            return "Executed " + self.name + " (general calibration). Ready for precise calibration."

        # Action cancelled
        else:
            print("Engine is not connected")
            return "Connect engine before executing " + self.name

    @staticmethod
    def precise_calibration(engine, engine_status):
        # Just wait for user calibration ends (no time for that) to write the value
        while engine_status.is_in_calibration():
            engine_status.wait_until_stop(2)
        # finish engine operations and write the final value
        current_degrees = MathsTools().get_degree_value(engine.get_absolute_present_position())
        Configuration.write_origin_degrees(current_degrees % 360)
        return current_degrees % 360

